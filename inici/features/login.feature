# language: es
Funcionalidad: Login
    Como usuario invitado (Guest)
    Quiero poder hacer login
    Para poder acceder a la parte privada de GestioCI

    Escenario: Tengo las credenciales correctas
        Dado que no estoy logeado
        Y que accedo a la pantalla de login
        Cuando relleno "Nom d'usuari" con "user"
        Y relleno "Contrasenya" con "user"
        Y clico el boton "Login"
        Entonces termino en el dashboard

    Escenario: Mi usuario y/o password es incorrecto
        Dado que no estoy logeado
        Y que accedo a la pantalla de login
        Cuando relleno "Nom d'usuari" con "user"
        Y relleno "Contrasenya" con "wr0ng.p4ss"
        Y clico el boton "Login"
        Entonces termino en "/accounts/login/"
        Y veo el texto "Si us plau, introduïu un nom d'usuari i clau."

    Escenario: Si intento entrar en una URL en concreto al hacer login deberia terminar en esa misma URL
        Dado que no estoy logeado
        Cuando accedo a "/socies/alta/autoocupat/"
        Entonces termino en "/accounts/login/?next=/socies/alta/autoocupat/"
        Cuando relleno "Nom d'usuari" con "user"
        Y relleno "Contrasenya" con "user"
        Y clico el boton "Login"
        Entonces termino en "/socies/alta/autoocupat/"
