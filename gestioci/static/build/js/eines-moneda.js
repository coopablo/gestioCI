function arrodonir_a_2_decimals(v) {
    // http://stackoverflow.com/a/6134070
    return parseFloat(Math.round(v * 100) / 100).toFixed(2);
}