GestioCI Frontend
=================

Los ficheros css/js/imágenes/fuentes… se gestionan a traves de este sub-proyecto. El script de build corre sobre [node.js](http://nodejs.org)/[io.js](http://iojs.org) y se encarga de compilar los Sass, combinar/minificar los Javascripts/CSS, optimizar imágenes estática, etc.

El resultado del script se guarda en la carpeta `build`.

TL/DR
-----

Previos:

    $ npm install -g bower gulp
    $ npm install && bower install

Desarrollo:

    $ python ../../manage.py runserver
    $ gulp

Dependencias
------------

1. [node.js](http://nodejs.org) o [io.js](http://iojs.org).
    - Puedes instalar/gestionar diferentes versiones de Node.js/io.js mediante [nvm](https://github.com/creationix/nvm).
    - Node.js/io.js trae un _package manager_ llamado `npm`, similar a `gem`, `pip` o `composer`. Utilizaremos el gestor para instalar las demas dependencias.
2. [Bower](http://bower.io). Es un _package manager_ escrito sobre Node.js que nos servira para gestionar las dependencias del frontend tipo librerias Javascript (jQuery, selectize.js…) o CSS (Foundation).
    - Instalar globalmente: `npm install -g bower`.
    - Una vez instalado podemos utilizar el comando `bower` para gestionar las dependencias.
3. [Gulp](http://gulpjs.com). Build system escrito sobre Node.js que se encargara de observar los ficheros y compilarlos cuando haya cambios, actualizar el navegador de forma automatica, etc.
    - Instalar globalmente: `npm install -g gulp`.
    - Una vez instalado tendremos un comando `gulp` para poner en marcha el build system.
4. Finalmente instalaremos las dependencias del proyecto, esto nos creara las carpetas `node_modules` y `bower_components`:
    - `npm install && bower install`

Desarrollo
----------

###Pon en marcha el build system

    $ gulp

Esto abrira un proceso que se quedara observando los css/js/plantillas/… e ira compilando los diferentes archivos segun detecte cambios. Para que funcione **el servidor de Django tiene que estar en marcha**.

Tambien pondra en marcha un servidor proxy de Django en `http://localhost:4000`. Si accedes a traves de esta dirección la pagina se refrescara automaticamente cada vez que detecte un cambio (cortesia de [BrowserSync](http://www.browsersync.io)).

###Añadir un nuevo javascript

1. Crear un nuevo archivo en la carpeta `js`. Ejemplo: `js/example.js`.
2. Editar `gulpfile.js` y añadir `js/example.js` en el array de Javascripts en `gulp.task('js')`. Ojo, el orden importa.
3. Parar `gulp` y volver a poner en marcha.

###Añadir una nueva dependencia de Bower

1. Puedes [buscar online](http://bower.io/search/) o en la consola: `bower search <term>`
2. Instalar: `$ bower install <package> --save`. Importante añadir `--save` para que actualize `bower.json`.
3. Editar `gulpfile.js`:
    - a) Si quieres añadir un Javascript, mira el apartado "Añadir un nuevo Javascript", solo que apunta a la carpeta `bower_components` en vez de `js`.
    - b) Si quieres añadir un Sass/CSS puedes editar `gulp.task('sass')`, añadir la ruta al package de bower en `includePaths` y hacer un include en el Sass con `@import`.
4. Parar `gulp` y volver a poner en marcha.

###Compilar y punto

    $ gulp build

Documentación librerias
-----------------------

- [Foundation](http://foundation.zurb.com/docs) (CSS)
- [Sass](http://sass-lang.com) (CSS)

Estructura
----------

- `bower_components/`: dependencias de Bower. Se gestionan con el comando `bower`.
- `build/`: carpeta donde se guardan los archivos compilados. Django leera esta carpeta.
- `js/`: Javascripts.
- `node_modules/`: dependencias de Node.js. Se gestionan con el comando `npm`.
- `scss/`: ficheros [Sass](http://sass-lang.com). Sass es un preprocesador CSS que permite utilizar variables/mixins/includes/… haciendo que el mantenimiento del CSS sea algo más agradable. Los ficheros que comienzan por `_` no se compilan; los demas se guardaran en la carpeta `build` convertidos a CSS (ej.: `gestio.scss` se convertira en `gestio.css`).
- `bower.json`: listado de dependencias de Bower. Similar a `requirements.txt`.
- `gulpfile.js`: Script de build de Gulp. Aqui se definen los diferentes _tasks_ que ha de hacer el build system.
- `package.json`: listado de dependencias de Node.js. Similar a `requirements.txt`.
