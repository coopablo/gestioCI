from django.contrib import admin

from .models import FacturaEmesa, FacturaRebuda, LiniaFactura, Trimestre, QuotaTrimestral

for model in (FacturaEmesa,
              FacturaRebuda,
              LiniaFactura,
              Trimestre,
              QuotaTrimestral):

    admin.site.register(model)
