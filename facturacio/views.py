# coding=utf-8
from decimal import Decimal

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.forms import inlineformset_factory, modelformset_factory, TextInput
from django.http.response import HttpResponseForbidden
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone

from facturacio.helpers import calcular_periode
from facturacio.models import LiniaFactura, Moviment
from projectes.models import ProjecteAutoocupat
from empreses.models import Empresa, Cooperativa
from .forms import FormCrearFacturaEmesa, FormConfirmarFacturaEmesa, FormCrearFacturaRebuda, \
    FormEditarFacturaRebuda, FormCercarValidacioFacturaRebuda, FormValidacioFacturaRebuda, FormSeleccioVolumFacturacio
from socies.models import PersonaUsuaria
from gestioci.settings import auth_groups
from .models import QuotaTrimestral, Factura, FacturaEmesa, FacturaRebuda, Trimestre, trobar_balanc_actual


def llistat_quotes_trimestrals(request):
    today = timezone.localtime(timezone.now()).date()

    dates_vigor = QuotaTrimestral.objects.order_by('-data_vigor').values_list('data_vigor').distinct()
    dates_vigor = [d[0] for d in dates_vigor]

    taules = []
    hem_passat_actual = 0

    for data_vigor in dates_vigor:

        taula = QuotaTrimestral.objects.filter(data_vigor=data_vigor).order_by('base_imposable_minima')

        if data_vigor > today:
            temps = 'futur'
        else:

            if data_vigor <= today and hem_passat_actual == 0:
                temps = 'present'
            else:
                temps = 'passat'

            hem_passat_actual += 1

        taules.append(dict(data_vigor=data_vigor,
                           taula=taula,
                           temps=temps))

        if hem_passat_actual > 1:
            break

    return render(
        request,
        'facturacio/llistat_quotes_trimestrals.html',
        dict(taules=taules)
    )


@login_required
def resum_facturacio_projecte(request, id_projecte=None):

    if id_projecte is None:
        projectes = _projectes(request.user)
        if not projectes:
            messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
            return redirect('inici')

        projecte = projectes[0]

    else:
        projectes = ProjecteAutoocupat.objects.filter(pk=id_projecte)

        if projectes.count():
                projecte = projectes[0]
        else:
            messages.error(request, u"No hi ha cap projecte autoocupat amb aquest identificador.")
            return redirect('inici')

        if request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists() or \
                projecte not in _projectes(request.user):

            messages.error(request, u"No tens permís per consultar la facturació de cap projecte autoocupat.")
            return redirect('inici')

    if 'periode' in request.GET:
        periode = request.GET.get('periode')
        (des_de, fins_a) = calcular_periode(periode)
    else:
        # assumim que el trimestre actual existeix ;)
        trimestre = Trimestre.trobar_trimestre(timezone.localtime(timezone.now()))
        periode = trimestre.nom
        des_de = trimestre.data_inici
        fins_a = trimestre.data_final

    if des_de:

        factures_emeses = projecte.factures_emeses(des_de, fins_a)
        factures_rebudes = projecte.factures_rebudes(des_de, fins_a)

        resum_emeses = factures_emeses.aggregate(
            Sum('base_total'),
            Sum('iva_total'),
            Sum('req_total'),
            Sum('import_total'))

        resum_rebudes = factures_rebudes.aggregate(
            Sum('base_total'),
            Sum('iva_total'),
            Sum('irpf_total'),
            Sum('import_total'))

    else:
        factures_emeses = FacturaEmesa.objects.none()
        factures_rebudes = FacturaRebuda.objects.none()
        resum_emeses = resum_rebudes = None

    return render(
        request,
        'facturacio/resum_facturacio_projecte.html',
        dict(
            projecte=projecte,
            periode=periode,
            des_de=des_de,
            fins_a=fins_a,
            factures_emeses=factures_emeses,
            factures_rebudes=factures_rebudes,
            resum_rebudes=resum_rebudes,
            resum_emeses=resum_emeses,
        )
    )


# això no és cap vista!!
def _projectes(user):
    try:
        persona = PersonaUsuaria.objects.get(usuari=user).persona
        resultat = persona.projectes_autoocupats.all()
    except PersonaUsuaria.DoesNotExist:
        resultat = ProjecteAutoocupat.objects.none()

    return resultat


@login_required
def veure_factura_emesa(request, id_factura):
    factura = get_object_or_404(FacturaEmesa, pk=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden(u"aquesta factura no és teva")

    # potser ho podem questionar, però de moment les proformes no es mostren; s'editen.
    if not factura.numero:
        if factura.liniafactura_set.count():
            return redirect('facturacio:confirmar_factura_emesa', id_factura=factura.pk)
        else:
            return redirect('facturacio:editar_linies_factura_emesa', id_factura=factura.pk)

    resum_iva, resum_req, resum_totals = factura.calcular_resums()

    return render(
        request,
        'facturacio/veure_factura_emesa.html',
        dict(
            f=factura,
            resum_iva=resum_iva,
            resum_req=resum_req,
            resum_totals=resum_totals,
        )
    )


@login_required
def crear_factura_emesa(request, id_client=None):
    projectes = _projectes(request.user)
    if not projectes:
        messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
        return redirect('inici')

    if projectes.count() > 1:
        messages.error(request, u"No està previst que una persona pugui facturar des de més d'un projecte.")
        return redirect('inici')
    else:
        projecte = projectes[0]

    clients_habituals = projecte.clients()
    initial = {}
    if id_client:
        client_proposat = Empresa.objects.filter(pk=id_client)  # vull una queryset pq despres ens convé la "unió"
        if client_proposat.count():
            initial['client'] = client_proposat[0].id
            clients_habituals |= client_proposat

    form = None

    if clients_habituals.count():

        if request.method == 'POST':

            form = FormCrearFacturaEmesa(clients=clients_habituals, initial=initial, data=request.POST)

            if form.is_valid():
                factura = form.save(commit=False)
                factura.projecte = projecte
                factura.proveidor = projecte.cooperativa_assignada
                factura.save()

                return redirect('facturacio:editar_linies_factura_emesa', id_factura=factura.pk)

        else:

            form = FormCrearFacturaEmesa(clients=clients_habituals, initial=initial)

    return render(
        request,
        'facturacio/crear_factura_emesa.html',
        dict(
            form=form,
        )
    )


@login_required
def editar_linies_factura_emesa(request, id_factura):
    factura = get_object_or_404(FacturaEmesa, pk=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden(u"aquesta factura no és teva")
    if not factura.pertany_a_trimestre_obert():
        return HttpResponseForbidden(
            u"aquesta factura no es pot editar per què pertany a un trimestre que ja està tancat")
    if factura.numero:
        return HttpResponseForbidden(u"aquesta factura ja no es pot editar")

    # TODO wouldn't it be nice to refactor all this crap out to views.py? to a custom Form or Formset?
    _extra_attrs = {'onchange': 'linia_modificada(event.target)',
                    'style': 'text-align:right;'}

    FormsetLinies = inlineformset_factory(FacturaEmesa,
                                          LiniaFactura,
                                          extra=3,
                                          fields=(
                                              'quantitat',
                                              'concepte',
                                              'preu_unitari',
                                              'tipus_iva',
                                              'tipus_req'),
                                          widgets={
                                              'quantitat': TextInput(attrs=_extra_attrs),
                                              'preu_unitari': TextInput(attrs=_extra_attrs),
                                              'tipus_iva': TextInput(attrs=_extra_attrs),
                                              'tipus_req': TextInput(attrs=_extra_attrs),
                                          })

    if request.method == 'POST':
        formset = FormsetLinies(data=request.POST, instance=factura)
        if formset.is_valid():
            formset.save()
            factura.save()  # força a actualitzar camps calculats

            if request.POST.get('anem_a') == 'seguent':
                return redirect('facturacio:confirmar_factura_emesa', id_factura=factura.pk)

            return redirect('facturacio:editar_linies_factura_emesa', id_factura=factura.pk)

    else:
        formset = FormsetLinies(instance=factura)

    return render(
        request,
        'facturacio/editar_linies_factura_emesa.html',
        dict(
            factura=factura,
            formset=formset
        )
    )


@login_required
def confirmar_factura_emesa(request, id_factura):
    factura = get_object_or_404(FacturaEmesa, pk=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden(u"aquesta factura no és teva")
    if not factura.pertany_a_trimestre_obert():
        return HttpResponseForbidden(u"aquesta factura pertany a un trimestre que ja està tancat")
    if factura.numero:
        return HttpResponseForbidden(u"aquesta factura ja no es pot editar")

    if request.method == 'POST':

        form = FormConfirmarFacturaEmesa(data=request.POST, instance=factura)
        if form.is_valid():
            form.save()
            factura.save()  # required to update calculated fields

            accio = request.POST.get('anem_a')
            if accio == 'enrera':
                redirect('facturacio:editar_linies_factura_emesa', id_factura=factura.pk)
            elif accio == 'aqui':
                return redirect('facturacio:confirmar_factura_emesa', id_factura=factura.pk)
            else:
                factura.assignar_numero_sequencia()
                return redirect('facturacio:resum_facturacio_projecte', id_projecte=factura.projecte.pk)

    else:

        form = FormConfirmarFacturaEmesa(instance=factura)

    resum_iva, resum_req, resum_totals = factura.calcular_resums()

    return render(
        request,
        'facturacio/confirmar_factura_emesa.html',
        dict(
            factura=factura,
            form=form,
            resum_iva=resum_iva,
            resum_req=resum_req,
            base_total=factura.base_total,
            iva_total=factura.iva_total,
            req_total=factura.req_total,
            import_total=factura.import_total
        )
    )


@login_required
def veure_factura_rebuda(request, id_factura):
    factura = get_object_or_404(FacturaRebuda, pk=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden(u"aquesta factura no és teva")

    resum_iva, resum_totals = factura.calcular_resums()

    return render(
        request,
        'facturacio/veure_factura_rebuda.html',
        dict(
            f=factura,
            resum_iva=resum_iva,
            resum_totals=resum_totals,
        )
    )


@login_required
def crear_factura_rebuda(request, id_proveidor=None):
    projectes = _projectes(request.user)
    if not projectes:
        messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
        return redirect('inici')

    if projectes.count() > 1:
        messages.error(request, u"No està previst que una persona pugui facturar des de més d'un projecte.")
        return redirect('inici')
    else:
        projecte = projectes[0]

    proveidors = projecte.proveidors()
    initial = {}
    if id_proveidor:
        proveidor_proposat = Empresa.objects.filter(pk=id_proveidor)  # vull una queryset pq despres ens convé la "unió"
        if proveidor_proposat.count():
            initial['proveidor'] = proveidor_proposat[0].id
            proveidors |= proveidor_proposat

    form = None

    if proveidors.count():

        if request.method == 'POST':

            form = FormCrearFacturaRebuda(proveidors=proveidors, initial=initial, data=request.POST)

            if form.is_valid():
                factura = form.save(commit=False)
                factura.projecte = projecte
                factura.client = projecte.cooperativa_assignada
                factura.save()

                return redirect('facturacio:editar_linies_factura_rebuda', id_factura=factura.pk)

        else:

            form = FormCrearFacturaRebuda(proveidors=proveidors, initial=initial)

    return render(
        request,
        'facturacio/crear_factura_rebuda.html',
        dict(
            form=form,
        )
    )


@login_required
def editar_linies_factura_rebuda(request, id_factura):
    factura = get_object_or_404(FacturaRebuda, pk=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden(u"aquesta factura no és teva")
    if not factura.pertany_a_trimestre_obert():
        return HttpResponseForbidden(
            u"aquesta factura no es pot editar per què pertany a un trimestre que ja està tancat")

    # TODO wouldn't it be nice to refactor all this crap out to views.py? to a custom Form or Formset?
    _extra_attrs = {'onchange': 'linia_modificada(event.target)',
                    'style': 'text-align:right;'}

    FormsetLinies = inlineformset_factory(FacturaRebuda,
                                          LiniaFactura,
                                          # formset=FormsetLiniaFacturaEmesa,
                                          extra=3,
                                          fields=(
                                              'quantitat',
                                              'concepte',
                                              'preu_unitari',
                                              'tipus_iva'),
                                          widgets={
                                              'quantitat': TextInput(attrs=_extra_attrs),
                                              'concepte': TextInput(attrs=_extra_attrs),
                                              'preu_unitari': TextInput(attrs=_extra_attrs),
                                              'tipus_iva': TextInput(attrs=_extra_attrs),
                                          })

    if request.method == 'POST':

        form = FormEditarFacturaRebuda(data=request.POST, instance=factura)
        if form.is_valid():
            form.save()

        formset = FormsetLinies(data=request.POST, instance=factura)
        if formset.is_valid():

            formset.save()
            factura.save()  # força a actualitzar camps calculats

            if request.POST.get('anem_a') == 'seguent':
                return redirect('facturacio:veure_factura_rebuda', id_factura=factura.pk)

            return redirect('facturacio:editar_linies_factura_rebuda', id_factura=factura.pk)

    else:
        formset = FormsetLinies(instance=factura)
        form = FormEditarFacturaRebuda(instance=factura)

    return render(
        request,
        'facturacio/editar_linies_factura_rebuda.html',
        dict(
            factura=factura,
            formset=formset,
            form=form,
        )
    )


@login_required
def dashboard_facturacio(request):
    projectes = _projectes(request.user)
    if not projectes:
        messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
        return redirect('inici')

    if projectes.count() > 1:
        messages.error(request, u"No està previst que una persona sigui membre de referència a més d'un projecte.")
        return redirect('inici')
    else:
        projecte = projectes[0]

    anys_actiu = Factura.objects.filter(projecte=projecte).distinct('data__year').dates('data', 'year')
    anys_actiu = [d.year for d in anys_actiu]
    anys_actiu.sort(reverse=True)

    if not anys_actiu:
        anys_actiu = [timezone.localtime(timezone.now()).year, ]

    # fabriquem una estructura de "nested dict's" amb anys, trimestres i recomptes de factures, d'aquesta manera:
    #  {
    #    2013: {'total': XXX, 'trimestres': {1: XXX, 2: XXX, 3: XXX, 4: XXX}},
    #    2014: {'total': XXX, 'trimestres': {1: XXX, 2: XXX, 3: XXX, 4: XXX}},
    #    2015: {'total': XXX, 'trimestres': {1: XXX, 2: XXX, 3: XXX, 4: XXX}}
    #  }
    # i cada XXX és un dict {'rebudes': n, 'emeses': m}

    activitat = dict()
    for a in anys_actiu:

        activitat[a] = dict(
            total=dict(emeses=0, rebudes=0),
            trimestres=dict())

        for num_t in [1, 2, 3, 4]:
            t = Trimestre.trobar_per_any_i_numero(a, num_t)
            ne = nr = 0
            if t:
                fe = FacturaEmesa.objects.filter(projecte=projecte)
                ne = fe.filter(data__gte=t.data_inici, data__lte=t.data_final).count()
                fr = FacturaRebuda.objects.filter(projecte=projecte)
                nr = fr.filter(data__gte=t.data_inici, data__lte=t.data_final).count()

            activitat[a]['total']['emeses'] += ne
            activitat[a]['total']['rebudes'] += nr
            activitat[a]['trimestres'][num_t] = dict(emeses=ne, rebudes=nr)

    balanc_ultim_trimestre, balanc_actual = trobar_balanc_actual(projecte)[0:2]

    # de cara al membre de referència, els balanços negatius es *mostren* al seu favor:
    balanc_actual = -balanc_actual
    if balanc_ultim_trimestre is not None:
        balanc_ultim_trimestre = -balanc_ultim_trimestre

    return render(
        request,
        'facturacio/dashboard_facturacio_projecte.html',
        dict(
            projecte=projecte,
            activitat=activitat,
            balanc_ultim_trimestre=balanc_ultim_trimestre,
            balanc_actual=balanc_actual,
        )
    )


@login_required
def llistat_moviments(request, id_projecte=None):

    if id_projecte is None:
        projectes = _projectes(request.user)
        if not projectes:
            messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
            return redirect('inici')

        projecte = projectes[0]

    else:
        if request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
            projectes = ProjecteAutoocupat.objects.filter(pk=id_projecte)
            if projectes.count():
                projecte = projectes[0]
            else:
                messages.error(request, u"No hi ha cap projecte autoocupat amb aquest identificador.")
                return redirect('inici')
        else:
            messages.error(request, u"No tens permís per consultar els comptes de cap projecte autoocupat.")
            return redirect('inici')

    # TODO permetre consulta de moviments de periodes anteriors
    # if 'periode' in request.GET:
    #     nom_trimestre = request.GET.get('periode')
    #     t = get_object_or_404(Trimestre, nom=nom_trimestre)

    # TODO permetre consulta de moviments de periodes anteriors
    # if 'periode' not in request.GET:
    #     assumim que el trimestre actual existeix ;)
    #     t = Trimestre.trobar_trimestre(timezone.localtime(timezone.now()))

    balanc_ultim_trimestre, balanc_actual, ultim_trimestre_tancat, data_inicial, data_final = trobar_balanc_actual(projecte)

    # de cara al membre de referència, els balanços negatius es *mostren* al seu favor:
    balanc_actual = -balanc_actual
    if balanc_ultim_trimestre is not None:
        balanc_ultim_trimestre = -balanc_ultim_trimestre
    else:
        balanc_ultim_trimestre = Decimal('0.00')

    saldo = balanc_ultim_trimestre
    linies = []

    if data_inicial:
        moviments = Moviment.objects.filter(projecte=projecte)
        moviments = moviments.filter(data__gte=data_inicial, data__lte=data_final)
        moviments = moviments.order_by('data', 'quantitat')  # ordenat per l'import evitem vermells que "no calen"

        for m in moviments:
            saldo -= m.quantitat  # no perdem de vista que tot és en negatiu quan es mostra a membres de referència
            concepte = m.empresa

            linies.append(dict(
                data=m.data,
                concepte=concepte,
                quantitat=-m.quantitat,
                saldo=saldo,
            ))

    assert saldo == balanc_actual

    return render(
        request,
        'facturacio/llistat_moviments.html',
        dict(
            balanc_ultim_trimestre=balanc_ultim_trimestre,
            balanc_actual=balanc_actual,
            ultim_trimestre_tancat=ultim_trimestre_tancat,
            linies=linies,
        )
    )


@login_required
def validacio_factures_rebudes(request):

    if not request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
        messages.error(request, u"No tens permís per validar factures.")
        return redirect('inici')

    FormValidacio = modelformset_factory(FacturaRebuda, form=FormValidacioFacturaRebuda, extra=0)

    form_validacio = None
    post_with_errors = False

    if request.method == 'POST':
        form_validacio = FormValidacio(data=request.POST)
        if form_validacio.is_valid():
            form_validacio.save()

        else:
            post_with_errors = True

    if 'cooperativa' in request.GET:  # there is a search criteria in place

        ordenar_per = request.GET.get('ordenar_per')

        form_cerca = FormCercarValidacioFacturaRebuda(data=request.GET)

        if not post_with_errors and form_cerca.is_valid():  # si hi havien errors al POST, no cal buscar factures

            factures = FacturaRebuda.objects.filter(client__id=request.GET.get('cooperativa'))

            mostrar_estat = request.GET.get('mostrar_estat')
            factures = factures.filter(estat=mostrar_estat)

            des_de = form_cerca.cleaned_data['des_de']
            if des_de:
                factures = factures.filter(data__gte=des_de)
            fins_a = form_cerca.cleaned_data['fins_a']
            if fins_a:
                factures = factures.filter(data__lte=fins_a)

            ordenar_per = form_cerca.cleaned_data['ordenar_per']
            if ordenar_per == FormCercarValidacioFacturaRebuda.OPCIO_PROJECTE:
                factures = factures.order_by('projecte__nom', 'data')
            elif ordenar_per == FormCercarValidacioFacturaRebuda.OPCIO_PROVEIDOR:
                factures = factures.order_by('proveidor__nom', 'data')
            else:
                factures = factures.order_by('data', 'projecte__nom', 'proveidor__nom')

            limit = form_cerca.cleaned_data['limit']

            factures = factures[:limit]

            form_validacio = FormValidacio(queryset=factures)

    else:  # when there is no search criteria in place

        ordenar_per = FormCercarValidacioFacturaRebuda.OPCIO_DATA

        form_cerca = FormCercarValidacioFacturaRebuda(initial=dict(
            ordenar_per=ordenar_per,
            mostrar_estat=FacturaRebuda.ESTAT_REBUDA,
            limit=20
        ))

    return render(
        request,
        'facturacio/validacio_factures_rebudes.html',
        dict(
            form_cerca=form_cerca,
            ordre=ordenar_per,
            form_validacio=form_validacio,

            ordenar_per_data=FormCercarValidacioFacturaRebuda.OPCIO_DATA,
            ordenar_per_proveidor=FormCercarValidacioFacturaRebuda.OPCIO_PROVEIDOR,
            ordenar_per_projecte=FormCercarValidacioFacturaRebuda.OPCIO_PROJECTE,

            estat_acceptada=FacturaRebuda.ESTAT_ACCEPTADA,
            estat_rebutjada=FacturaRebuda.ESTAT_REBUTJADA,
            estat_rebuda=FacturaRebuda.ESTAT_REBUDA,
        )
    )


@login_required
def llistat_projectes_autoocupats(request):

    if not request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
        messages.error(request, u"No tens permís per accedir a aquesta informació.")
        return redirect('inici')

    projectes = []
    for p in ProjecteAutoocupat.objects.all().order_by('nom'):

        balanc_ultim_trimestre, balanc_actual = trobar_balanc_actual(p)[0:2]

        projectes.append(dict(
            instance=p,
            balanc_ultim_trimestre=balanc_ultim_trimestre,
            balanc_actual=balanc_actual,
        ))

    return render(
        request,
        'facturacio/llistat_projectes_autoocupats.html',
        dict(
            projectes=projectes
        )
    )


def volum_facturacio_empresa(request, tipus):

    if not request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
        messages.error(request, u"No tens permís per accedir a aquesta informació.")
        return redirect('inici')

    if tipus not in ['client', 'proveidor']:
        messages.error(request, u"Oops! sembla que hi ha un problema al programa... o que jugues a alguna cosa rara.")
        return redirect('inici')

    if tipus == 'client':
        factures = FacturaEmesa.objects.all()
    else:
        factures = FacturaRebuda.objects.all()

    if 'periode' in request.GET:
        periode = request.GET.get('periode')
        (des_de, fins_a) = calcular_periode(periode)
        if not des_de or not fins_a:
            messages.error(request, u"OOps! el periode no està correctament especificat.")
            return redirect('inici')

    else:
        trimestre = Trimestre.trobar_trimestre(timezone.localtime(timezone.now()))
        periode = trimestre.nom
        des_de = trimestre.data_inici
        fins_a = trimestre.data_final

    factures = factures.filter(data__gte=des_de)
    factures = factures.filter(data__lte=fins_a)

    # si no s'especifica un id de cooperativa, es calcularan
    # els totals acumulats entre totes les cooperatives
    if 'cooperativa' in request.GET:
        id_cooperativa = request.GET.get('cooperativa')
    else:
        # TODO potser cal alguna manera menys arbitrària de triar la cooperativa per defecte xD
        id_cooperativa = 1

    if not Cooperativa.objects.filter(pk=id_cooperativa).exists():
        messages.error(request, u"No hi ha cap cooperativa amb aquest identificador.")
        return redirect('inici')

    if tipus == 'client':
        factures = factures.filter(proveidor__pk=id_cooperativa)
    else:
        factures = factures.filter(client__id=id_cooperativa)

    totals = dict(
        base=Decimal('0.00'),
        iva=Decimal('0.00'),
        irpf=Decimal('0.00'),
        req=Decimal('0.00'),
        total=Decimal('0.00')
    )

    # 'empreses' és un diccionari amb empresa.id com a clau i un diccionari com a valor
    # aquest segon diccionari conté els totals acumulats per tota la cooperativa, o per
    # totes les cooperatives, segons s'hagi demanat
    empreses = dict()
    for factura in factures:
        if tipus == 'client':
            empresa = factura.client
        else:
            empresa = factura.proveidor

        if empresa.id in empreses:
            acumulat = empreses.get(empresa.id)
        else:
            acumulat = dict(
                instance=empresa,
                base=Decimal('0.00'),
                iva=Decimal('0.00'),
                irpf=Decimal('0.00'),
                req=Decimal('0.00'),
                total=Decimal('0.00')
            )

        acumulat['base'] += factura.base_total
        acumulat['iva'] += factura.iva_total
        acumulat['total'] += factura.import_total

        totals['base'] += factura.base_total
        totals['iva'] += factura.iva_total
        totals['total'] += factura.import_total

        if tipus == 'client':
            acumulat['req'] += factura.req_total
            totals['req'] += factura.req_total
        else:
            acumulat['irpf'] += factura.irpf_total
            totals['irpf'] += factura.irpf_total

        empreses[empresa.id] = acumulat

    return render(
        request,
        'facturacio/volum_facturacio_empresa.html',
        dict(
            periode=periode,
            tipus=tipus,
            empreses=empreses.values(),
            totals=totals,
            form_seleccio=FormSeleccioVolumFacturacio(initial=dict(periode=periode, cooperativa=id_cooperativa))
        )
    )
