# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings

from gestioci.settings import auth_groups


def crear_grup_responsables_facturacio(apps, schema_editor):
    """
    Aquest mètode serveix com a plantilla per crear grups al futur;
    """
    Group = apps.get_model('auth', 'Group')

    verbose = False
    noms_dels_grups = [auth_groups.RESPONSABLES_FACTURACIO, ]

    if verbose:
        print()

    for nom in noms_dels_grups:
        try:
            u = Group.objects.get(name=nom)
            if verbose:
                print '''    = "%s": el grup ja existeix''' % nom
        except Group.DoesNotExist:
            Group.objects.create(name=nom)
            if verbose:
                print '''    + "%s": el grup no existia però s'ha creat''' % nom


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('empreses', '0003_empresa_adreca_fiscal'),
        ('projectes', '0001_initial'),
        ('facturacio', '0004_auto_20150825_1933'),
    ]

    operations = [
        migrations.CreateModel(
            name='BalancTancamentTrimestre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('balanc_final', models.DecimalField(default=0, max_digits=8, decimal_places=2)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
                ('projecte', models.ForeignKey(related_name='+', to='projectes.ProjecteAutoocupat')),
                ('trimestre', models.ForeignKey(related_name='+', to='facturacio.Trimestre')),
                ('updated_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Balan\xe7 al tancament de trimestre',
                'verbose_name_plural': 'Balan\xe7os al tancament de trimestre',
            },
        ),
        migrations.CreateModel(
            name='Moviment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantitat', models.DecimalField(default=0, max_digits=8, decimal_places=2)),
                ('data', models.DateField()),
                ('detalls', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('cooperativa', models.ForeignKey(related_name='+', to='empreses.Cooperativa')),
                ('created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
                ('empresa', models.ForeignKey(related_name='+', blank=True, to='empreses.Empresa', null=True)),
                ('projecte', models.ForeignKey(to='projectes.ProjecteAutoocupat')),
                ('updated_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RunPython(crear_grup_responsables_facturacio),
    ]
