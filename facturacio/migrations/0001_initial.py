# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('empreses', '0002_auto_20150629_1815'),
    ]

    operations = [
        migrations.CreateModel(
            name='Factura',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.DateField()),
                ('year', models.SmallIntegerField()),
                ('numero', models.CharField(max_length=64)),
                ('import_total', models.DecimalField(default=0, max_digits=8, decimal_places=2)),
                ('iva_total', models.DecimalField(default=0, max_digits=8, decimal_places=2)),
                ('data_venciment', models.DateField(null=True, blank=True)),
                ('data_prevista_pagament', models.DateField(null=True, blank=True)),
                ('forma_de_pagament', models.CharField(default=b'efectiu', max_length=16, choices=[(b'efectiu', 'En efectiu'), (b'transferencia', 'Gestiona la cooperativa')])),
            ],
            options={
                'verbose_name_plural': 'factures',
            },
        ),
        migrations.CreateModel(
            name='LiniaFactura',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantitat', models.DecimalField(max_digits=8, decimal_places=2)),
                ('concepte', models.CharField(max_length=128)),
                ('preu_unitari', models.DecimalField(max_digits=8, decimal_places=2)),
                ('tipus_iva', models.SmallIntegerField()),
            ],
            options={
                'verbose_name': 'l\xednia de factura',
                'verbose_name_plural': 'l\xednies de factura',
            },
        ),
        migrations.CreateModel(
            name='QuotaTrimestral',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('base_imposable_minima', models.DecimalField(max_digits=8, decimal_places=2)),
                ('import_quota_trimestral', models.DecimalField(max_digits=8, decimal_places=2)),
                ('data_vigor', models.DateField(verbose_name="data d'entrada en vigor")),
            ],
        ),
        migrations.CreateModel(
            name='Trimestre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(unique=True, max_length=10)),
                ('data_inici', models.DateField()),
                ('data_final', models.DateField()),
                ('data_limit_tancament', models.DateField()),
                ('obert', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='FacturaEmesa',
            fields=[
                ('factura_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='facturacio.Factura')),
                ('estat', models.CharField(default=b'emesa', max_length=16, choices=[(b'emesa', 'Pendent de cobrar'), (b'cobrada', 'Cobrada')])),
                ('recarreg_equivalencia', models.DecimalField(default=0, verbose_name="rec\xe0rreg d'equival\xe8ncia", max_digits=8, decimal_places=2)),
            ],
            options={
                'verbose_name_plural': 'factures emeses',
            },
            bases=('facturacio.factura',),
        ),
        migrations.CreateModel(
            name='FacturaRebuda',
            fields=[
                ('factura_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='facturacio.Factura')),
                ('estat', models.CharField(default=b'rebuda', max_length=16, choices=[(b'rebuda', 'Pendent de revisar'), (b'acceptada', 'Pendent de pagar'), (b'pagada', 'Pagada')])),
                ('retencio_irpf', models.DecimalField(default=0, verbose_name='retenci\xf3 IRPF', max_digits=8, decimal_places=2)),
            ],
            options={
                'verbose_name_plural': 'factures rebudes',
            },
            bases=('facturacio.factura',),
        ),
        migrations.AddField(
            model_name='liniafactura',
            name='factura',
            field=models.ForeignKey(to='facturacio.Factura'),
        ),
        migrations.AddField(
            model_name='factura',
            name='client',
            field=models.ForeignKey(related_name='factures_rebudes', to='empreses.Empresa'),
        ),
    ]
