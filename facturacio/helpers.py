# coding=utf-8
from datetime import date
from calendar import monthrange
import re


def calcular_trimestre(data):
    """
    Retorna (any, trimestre, inici, final) per al trimestre que inclou la data especificada.
    Trimestre és un enter d'aquests: [1, 2, 3, 4]
    """
    year = data.year
    trimestre = [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4][data.month-1]
    primer_mes = (trimestre - 1) * 3 + 1
    ultim_mes = primer_mes + 2
    inici = date(year, primer_mes, 1)
    final = date(year, ultim_mes, monthrange(year, ultim_mes)[1])
    return year, trimestre, inici, final


re_trimestre = re.compile(r'\d{4}T[1-4]\Z', re.IGNORECASE)  # e.g. 2015T2
re_any = re.compile(r'\d{4}\Z')  # e.g. 2015


def calcular_periode(spec):
    """
    Retorna dues dates (de calendari, sense rellotge) que corresponen a l'inici i final del periode;
    els periodes es poden especificar:
    - amb el nom d'un trimestre, p.ex. 2015T2
    - amb un any, p.ex. 2015
    """
    if re_trimestre.match(spec):
        from facturacio.models import Trimestre
        try:
            trimestre = Trimestre.objects.get(nom=spec.upper())
            return trimestre.data_inici, trimestre.data_final
        except Trimestre.DoesNotExist:
            return None, None

    if re_any.match(spec):
        year = int(spec)
        if year > 1900:
            return date(year, 1, 1), date(year, 12, 31)

    return None, None
