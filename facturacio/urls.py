from django.conf.urls import patterns, include, url

urlpatterns = patterns(

    'facturacio.views',

    url(r'^llistat_quotes_trimestrals/$',                    'llistat_quotes_trimestrals',   name='llistat_quotes_trimestrals'),

    url(r'^crear_factura_emesa/$',                           'crear_factura_emesa',          name='crear_factura_emesa'),
    url(r'^crear_factura_emesa/(?P<id_client>\d+)$',         'crear_factura_emesa',          name='crear_factura_emesa'),
    url(r'^linies_factura_emesa/(?P<id_factura>\d+)$',       'editar_linies_factura_emesa',  name='editar_linies_factura_emesa'),
    url(r'^confirmar_factura_emesa/(?P<id_factura>\d+)$',    'confirmar_factura_emesa',      name='confirmar_factura_emesa'),
    url(r'^veure_factura_emesa/(?P<id_factura>\d+)$',        'veure_factura_emesa',          name='veure_factura_emesa'),

    url(r'^crear_factura_rebuda/$',                          'crear_factura_rebuda',         name='crear_factura_rebuda'),
    url(r'^crear_factura_rebuda/(?P<id_proveidor>\d+)$',     'crear_factura_rebuda',         name='crear_factura_rebuda'),
    url(r'^linies_factura_rebuda/(?P<id_factura>\d+)$',      'editar_linies_factura_rebuda', name='editar_linies_factura_rebuda'),
    url(r'^veure_factura_rebuda/(?P<id_factura>\d+)$',       'veure_factura_rebuda',         name='veure_factura_rebuda'),
    url(r'^validacio_factures_rebudes/$',                    'validacio_factures_rebudes',   name='validacio_factures_rebudes'),

    url(r'^resum_facturacio_projecte/$',                     'resum_facturacio_projecte',    name='resum_facturacio_projecte'),
    url(r'^resum_facturacio_projecte/(?P<id_projecte>\d+)$', 'resum_facturacio_projecte',    name='resum_facturacio_projecte'),

    url(r'^moviments/$',                                     'llistat_moviments',            name='moviments'),
    url(r'^moviments/(?P<id_projecte>\d+)$',                 'llistat_moviments',            name='moviments'),

    url(r'^llistat_projectes_autoocupats/$',                 'llistat_projectes_autoocupats', name='llistat_projectes_autoocupats'),

    url(r'^facturacio_per_proveidor/$', 'volum_facturacio_empresa', kwargs=dict(tipus='proveidor'), name='facturacio_per_proveidor'),
    url(r'^facturacio_per_client/$',    'volum_facturacio_empresa', kwargs=dict(tipus='client'),    name='facturacio_per_client'),


)
