from django.conf.urls import patterns, include, url

from .views import SessioMonedaListView, SessioMonedaCreateView
from .views import SessioAcollidaListView, SessioAcollidaCreateView, \
    SessioAcollidaUpdateView, SessioMonedaUpdateView
from .views import ProcesAltaAutoocupatDetailView

urlpatterns = patterns(

    'alta_socies.views',

    # -----------------
    # SOCIS AUTOOCUPATS
    # -----------------

    url(r'^rebutjar_proces_alta/(?P<id_proces>\d+)/$', 'rebutjar_proces_alta_autoocupat', name='rebutjar_proces'),

    url(r'^exportar_a_csv/$', 'exportar_a_csv', name='exportar_a_csv'),

    url(r'^autoocupat/resum/(?P<pk>\d+)/$', ProcesAltaAutoocupatDetailView.as_view(), name='resum_proces_alta_projecte_autoocupat'),

    url(r'^editar_sessio_moneda/(?P<pk>\d+)/$', SessioMonedaUpdateView.as_view(), name='editar_sessio_moneda'),

    url(r'^editar_sessio_acollida/(?P<pk>\d+)/$', SessioAcollidaUpdateView.as_view(), name='editar_sessio_acollida'),

    url(r'^llistat_sessio_acollida', SessioAcollidaListView.as_view(), name='llistat_sessio_acollida'),

    url(r'^crear_sessio_acollida', SessioAcollidaCreateView.as_view(), name='crear_sessio_acollida'),

    url(r'^llistat_sessio_moneda', SessioMonedaListView.as_view(), name='llistat_sessio_moneda'),

    url(r'^crear_sessio_moneda', SessioMonedaCreateView.as_view(), name='crear_sessio_moneda'),

    url(r'^generador_sessions/$',
        'generar_sessions',
        name='generador_sessions'),

    url(r'^sessions_del_mes/(?P<tipus>\w+)(/(?P<year>\d\d\d\d)/(?P<month>\d\d?))?$',
        'sessions_del_mes',
        name='sessions_del_mes'),

    url(r'^autoocupat/$',
        'proces_alta_projecte_autoocupat',
        name='proces_alta_projecte_autoocupat'),

    url(r'^autoocupat/(?P<id_proces>\d+)/$',
        'proces_alta_projecte_autoocupat',
        name='proces_alta_projecte_autoocupat'),

    url(r'^autoocupat/(?P<id_proces>\d+)/(?P<pas>\d+)/$',
        'proces_alta_projecte_autoocupat',
        name='proces_alta_projecte_autoocupat'),

    url(r'^autoocupat/llista-tasques/(?P<id_proces>\d+)/$',
        'proces_alta_projecte_autoocupat_llista_de_tasques',
        name='proces_alta_projecte_autoocupat-llista_de_tasques'),

    url(r'^autoocupat/llistat_proces_alta_autoocupat/$',
        'llistat_proces_alta_autoocupat',
        name='llistat_proces_alta_autoocupat'),

    url(r'^autoocupat/control-qualitat/$',
        'llistat_proces_alta_projecte_autoocupat_revisio_descripcions',
        name='llistat_proces_alta_projecte_autoocupat_revisio_descripcions'),

    url(r'^estadistiques_proces_alta_projecte_autoocupat/?$',
        'estadistiques_proces_alta_projecte_autoocupat',
        name='estadistiques_proces_alta_projecte_autoocupat'),

    url(r'^reassignacio_proces_alta_autoocupat/?$',
        'reassignacio_proces_alta_autoocupat',
        name='reassignacio_proces_alta_autoocupat'),

    # -----------------
    # SOCIS COOPERATIUS
    # -----------------
    url(r'^cooperatiu/$', 'cooperatiu', name='cooperatiu'),
)
