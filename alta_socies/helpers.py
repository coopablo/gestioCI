# coding=utf-8
from django.contrib.auth.models import Group, User
from alta_socies.models import ProcesAltaAutoocupat
from gestioci.settings import auth_groups


def responsables_projecte_autoocupat():
    """
    Retorna els usuaris que actualment poden ser responsables d'alta + els que històricament
    han estat responsables d'alta a processos d'alta que encara es troben "en curs".
    :return: QuerySet de User
    """

    grup = Group.objects.get(name=auth_groups.RESPONSABLES_ALTA)
    
    responsables_possibles = grup.user_set.values_list('id', flat=True)

    responsables_actuals = ProcesAltaAutoocupat.objects.filter(resolucio=ProcesAltaAutoocupat.RESOLUCIO_EN_CURS)\
        .distinct('responsable').values_list('responsable', flat=True)

    responsables = list(set(responsables_possibles) | set(responsables_actuals))
    
    return User.objects.filter(id__in=responsables)
